import React from 'react';
import { getProductById, Product } from '../../api-calls/product';
import Header from '../../components/Header';
import { ProductContent } from '../../containers/Product';

interface ProductPageProps {
	product: Product;
}

export default function ProductPage(props: ProductPageProps): React.ReactElement {
	return (
		<>
			<Header />
			<ProductContent product={props.product} />
		</>
	);
}

export async function getServerSideProps(context): Promise<{ props: any }> {
	const data = await getProductById(context.params.product_id);
	return {
		props: {
			product: { ...data },
		},
	};
}
