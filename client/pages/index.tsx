import React, { ReactElement } from 'react';
import { HomeComponent } from '../containers/Home';
import Header from '../components/Header';
import { GetServerSidePropsResult, NextPageContext } from 'next';
import { addProductToCart, getUserOrder } from '../api-calls/cart';
import { checkAuth, User } from '../api-calls/auth';

interface HomeProps {
	order: any;
	user: User;
}

export default function Home(props: HomeProps): ReactElement {
	function addToCart(product) {
		return addProductToCart(props.order.id, product);
	}

	return (
		<>
			<Header />
			<HomeComponent addToCart={addToCart} order={props.order} />
		</>
	);
}

export async function getServerSideProps(
	ctx: NextPageContext
): Promise<GetServerSidePropsResult<HomeProps>> {
	try {
		const order = await getUserOrder(ctx.req.headers.cookie);
		// console.log('order', order);
		const user = await checkAuth(ctx.req.headers.cookie);
		return {
			props: {
				order: order,
				user: user,
			},
		};
	} catch {
		return {
			redirect: {
				permanent: true,
				destination: '/login',
			},
		};
	}
}
