import React from 'react';
import CartContainer from '../../containers/Cart';
import { checkAuth, User } from '../../api-calls/auth';
import { GetServerSidePropsResult, NextPageContext } from 'next';
import { getUserOrder } from '../../api-calls/cart';

interface OrderPageProps {
	user: User;
	order: unknown;
}

export default function OrderPage(props: OrderPageProps): React.ReactElement {
	return (
		<div>
			<CartContainer order={props.order} user={props.user} />
		</div>
	);
}

export async function getServerSideProps(
	ctx: NextPageContext
): Promise<GetServerSidePropsResult<OrderPageProps>> {
	const cookies = ctx.req.headers.cookie;
	const user = await checkAuth(cookies);
	if (!user) {
		return {
			redirect: {
				destination: '/login',
				permanent: false,
			},
		};
	}
	const order = await getUserOrder(cookies);
	return {
		props: {
			user,
			order,
		},
	};
}
