import React from 'react';

import { ProfileContainer } from '../../containers/Profile/profile';
import { checkAuth, User } from '../../api-calls/auth';
import { NextPageContext } from 'next';

interface ProfileProps {
	user: User;
}

export default function ProfilePage(props: ProfileProps): React.ReactElement {
	return (
		<div>
			<ProfileContainer user={props.user} />
		</div>
	);
}

export async function getServerSideProps(ctx: NextPageContext): Promise<{ props: ProfileProps }> {
	const user = await checkAuth(ctx.req.headers.cookie);

	return {
		props: { user },
	};
}
