import React from 'react';
import { Elements } from '@stripe/react-stripe-js';
import config from '../../../config';
import { loadStripe } from '@stripe/stripe-js';
import AddCard from '../../../containers/Profile/add-card';
import { checkAuth, User } from '../../../api-calls/auth';
import { GetServerSidePropsResult, NextPageContext } from 'next';

const stripePromise = loadStripe(config.stripePublicKey);
interface AddCardProps {
	user: User;
}
export default function AddCardPage(): React.ReactElement {
	// console.log("AddCardPage props ===>>>", props)
	return (
		<Elements stripe={stripePromise}>
			<AddCard />
		</Elements>
	);
}

export async function getServerSideProps(
	ctx: NextPageContext
): Promise<GetServerSidePropsResult<AddCardProps>> {
	const user = await checkAuth(ctx.req.headers.cookie);

	if (!user) {
		return {
			redirect: {
				destination: '/login',
				permanent: true,
			},
		};
	}
	return {
		props: { user },
	};
}
