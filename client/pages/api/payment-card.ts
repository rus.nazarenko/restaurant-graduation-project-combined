import express from 'express';
import { getUserFromCookie } from './auth';
import { saveCard } from '../../private/card';
import { updateUser } from '../../private/user';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const stripe = require('stripe')(process.env.STRIPE_SECRET);

export default async function handler(req: express.Request, res: express.Response) {
	if (req.method === 'POST') {
		let user = await getUserFromCookie(req);
		const payload = req.body;
		if (!user.stripe_id) {
			user.stripe_id = (
				await stripe.customers.create({
					email: user.email,
					name: `${user.firstName} ${user.lastName}`,
				})
			).id;
			user = await updateUser(user);
		}

		const card = await saveCard({
			external_id: (
				await stripe.customers.createSource(user.stripe_id, {
					source: payload.token,
				})
			).id,
			user_id: user.id,
			updated_at: new Date(),
			created_at: new Date(),
		});

		res.status(200).json({ card });
	}
}
