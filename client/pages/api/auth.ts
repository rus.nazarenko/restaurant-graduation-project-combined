import jwt from 'jsonwebtoken';
import { getUser } from '../../private/user';
import express from 'express';
import { getDefaultCard } from '../../private/card';
import { User } from '../../api-calls/auth';

export async function getUserFromCookie(req: express.Request): Promise<User> {
	let decoded;

	try {
		decoded = jwt.verify(req.cookies['tso_token'], process.env.JWT_SECRET);
		// console.log(decoded);
	} catch {
		throw new Error('not valid token');
	}
	const user = await getUser(decoded.id);

	return { ...user, card: await getDefaultCard(user.id) };
}

export default async function handler(req: express.Request, res: express.Response) {
	try {
		const user = await getUserFromCookie(req);
		res.status(200).json({ user });
	} catch (e) {
		res.status(403).json({ error: e.message });
	}
}
