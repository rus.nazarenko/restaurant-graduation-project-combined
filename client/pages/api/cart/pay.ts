import express from 'express';
import { getUserFromCookie } from '../auth';
import { getOrderById, setOrder } from '../../../private/order';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const stripe = require('stripe')(process.env.STRIPE_SECRET);

export default async function handler(req: express.Request, res: express.Response) {
	if (req.method === 'POST') {
		const user = await getUserFromCookie(req);
		const order = await getOrderById(req.body.order_id);
		if (user.id !== order.user_id) {
			throw new Error('Can not pay for wrong order');
		}
		const amount = order.products.reduce((acc, it) => {
			acc += it.price;
			return acc;
		}, 0);
		const charge = await stripe.charges.create({
			source: user.card.external_id,
			amount: amount * 100,
			currency: 'usd',
			customer: user.stripe_id,
			description: `User payment of order ${order.id}`,
		});

		order.paid = true;

		await setOrder(order);

		res.status(200).json({ charge });
	}
}
