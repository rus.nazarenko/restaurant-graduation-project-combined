import express from 'express';
import { createOrder, getUserOrders } from '../../../private/order';
import { getUserFromCookie } from '../auth';

export default async function handler(
	req: express.Request,
	res: express.Response
): Promise<unknown> {
	async function getUserOrder(): Promise<unknown> {
		let user;
		try {
			user = await getUserFromCookie(req);
		} catch (e) {
			return res.status(403).json({ error: e.message });
		}
		const orders = await getUserOrders(user.id);
		let toSend = orders.find((it) => !it.paid);

		if (!toSend) {
			toSend = await createOrder(user.id, []);
		}
		res.status(200).json({ order: toSend });
	}

	switch (req.method) {
		case 'GET':
			await getUserOrder();
			break;

		default:
			return res.status(404).json({ error: 'Unsupported' });
	}
}
