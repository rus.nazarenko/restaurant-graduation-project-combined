import express from 'express';
import { getUserFromCookie } from '../auth';
import { addProductToOrder, getOrderById } from '../../../private/order';

export default async function handler(
	req: express.Request,
	res: express.Response
): Promise<unknown> {
	if (req.method === 'POST') {
		const user = await getUserFromCookie(req);
		const payload = req.body;
		let order = await getOrderById(payload.cart_id);
		if (user.id !== order.user_id) {
			return res.status(403).json({ error: 'You do not own this cart' });
		}

		const product = payload.product;

		order = await addProductToOrder(order.id, product);

		res.status(200).json({ order: order });
	}
}
