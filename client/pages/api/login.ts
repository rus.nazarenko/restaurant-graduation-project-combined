import express from 'express';
import * as jwt from 'jsonwebtoken';
import { getUserByEmail } from '../../private/user';

export default async function handler(req: express.Request, res: express.Response) {
	if (req.method === 'POST') {
		const userData = req.body;
		const user = await getUserByEmail(userData.email);
		if (user.password === userData.password) {
			res.status(200).json({
				user: {
					...user,
					access_token: jwt.sign({ id: user.id, expiresIn: '2d' }, process.env.JWT_SECRET),
				},
			});
		} else {
			res.status(400).json({ error: 'wrong pass' });
		}
	}
}
