import { getUser, updateUser } from '../../../private/user';
import express from 'express';

export default async function handler(req: express.Request, res: express.Response) {
	const { user_id } = req.query;

	if (req.method === 'PATCH') {
		const user = req.body;

		if (!user_id) {
			return res.status(400).json({ error: 'No user provided' });
		}

		const updatedUser = await updateUser({ ...user, id: user_id });
		res.status(200).json({ user: updatedUser });
	}

	if (req.method === 'GET') {
		const user = await getUser(+user_id);
		if (user) {
			return res.status(201).json(user);
		} else {
			res.status(404).json({ error: 'Not found' });
		}
	}
}
