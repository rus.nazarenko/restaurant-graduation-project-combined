import * as jwt from 'jsonwebtoken';
import { createUser } from '../../private/user';
const secret = process.env.JWT_SECRET;

export default async function handler(req, res) {
	if (req.method === 'POST') {
		const user = await createUser(req.body);
		const access_token = jwt.sign({ id: user.id, expiresIn: '2d' }, secret);
		res.setHeader('Set-Cookie', `tso_token=${access_token}; HttpOnly`);
		res.status(201).json({ ...req.body, access_token });
	}
}
