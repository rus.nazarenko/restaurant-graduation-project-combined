import React from 'react';
import LoginContainer from '../../containers/Login';

export default function LoginPage(): React.ReactElement {
	return (
		<div>
			<LoginContainer />
		</div>
	);
}
