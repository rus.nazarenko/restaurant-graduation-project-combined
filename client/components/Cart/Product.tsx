import React from 'react';

import styles from './styles.module.scss';

import { Product } from '../../api-calls/product';
import config from '../../config';

interface ProductProps {
	product: Product;
	count: number;
}

export default function ProductItem(props: ProductProps): React.ReactElement {
	return (
		<div className={styles.productItemWrapper}>
			<img alt={props.product.name} src={props.product.image || config.defaultImageUrl} />
			<p>{props.product.name}</p>
			<span>{props.count}</span>
		</div>
	);
}
