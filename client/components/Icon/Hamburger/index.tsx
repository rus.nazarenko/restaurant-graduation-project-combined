import React from 'react';

import styles from './styles.module.scss';

interface IProps {
	onClick: (action: any) => void;
}

export default function Hamburger(props: IProps): React.ReactElement {
	return (
		<div id={'menu-icon'} onClick={props.onClick} className={styles.menuContainer}>
			<div className={styles.bar} />
			<div className={styles.bar} />
			<div className={styles.bar} />
		</div>
	);
}
