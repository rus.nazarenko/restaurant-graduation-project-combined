import React, { ChangeEvent } from 'react';
import styles from './styles.module.scss';

interface InputProps {
	onChange: (e: React.ChangeEvent & { target: { value: string } }) => void;
	value: string;
	label: string;
	type?: string;
	name: string;

	placeholder: string;
}

export default function Input(props: InputProps): React.ReactElement {
	return (
		<div className={styles.inputWrapper}>
			<label>{props.label}</label>
			<input
				onChange={props.onChange}
				type={props.type || 'text'}
				placeholder={props.placeholder}
				className={styles.inputElement}
				value={props.value}
			/>
		</div>
	);
}
