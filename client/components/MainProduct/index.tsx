import React from 'react';
import Link from 'next/link';
import config from '../../config';
import styles from './style.module.scss';
import Button from '../Button/Button';
import { useRouter } from 'next/router';
import { Product } from '../../api-calls/product';

interface IProps {
	product: Product;
	onAdd: (product: { id: number }) => void;
}

export function MainProduct(props: IProps): React.ReactElement {
	const { product } = props;
	const router = useRouter();

	function onProductClick(): void {
		router.push(`/product/${product.id}`);
	}

	function addProduct(ev) {
		ev.preventDefault();

		props.onAdd(product);
	}

	return (
		<div className={styles.mainProductContainer}>
			<img
				className={styles.productImage}
				alt={product.name}
				src={(product.image || config.defaultImageUrl) as any}
			/>
			<div className={styles.infoWrapper}>
				<div>{product.description}</div>
				<Link href={`/product/${product.id}`}>
					<a>{product.name}</a>
				</Link>
			</div>
			<div className={styles.buttonsBlock}>
				<div className={styles.leftColumn}>
					<div className={styles.leftButton}>
						<Button onClick={onProductClick} style={'underlineStyle'} label="CUSTOMIZE" />
					</div>
				</div>
				<div className={styles.rightColumn}>
					<div className={styles.doubleButton}>
						<Button
							id={`add-to-cart-${product.id}`}
							style={'redBtn'}
							label={`Add to Cart $${product.price}`}
							onClick={addProduct}
						/>
					</div>
				</div>
			</div>
		</div>
	);
}
