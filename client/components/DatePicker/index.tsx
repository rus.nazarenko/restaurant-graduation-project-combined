import { useState } from 'react';
import ReactDatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

export default function DatePicker(): JSX.Element {
	const [startDate, setStartDate] = useState(new Date());

	return (
		<ReactDatePicker
			selected={startDate}
			onChange={(date) => setStartDate(date as any)}
			showTimeSelect
			timeFormat="HH:mm"
			// injectTimes={[
			// 	setHours(setMinutes(new Date(), 1), 0),
			// 	setHours(setMinutes(new Date(), 5), 12),
			// 	setHours(setMinutes(new Date(), 59), 23),
			// ]}
			dateFormat="MMMM d, yyyy h:mm aa"
		/>
	);
}
