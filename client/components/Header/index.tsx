import React, { useState } from 'react';
import cx from 'classnames';

import styles from './styles.module.scss';
import Hamburger from '../Icon/Hamburger';
import Navigation from './Navigation';

export default function Header(): JSX.Element {
	const [navOpen, toggleNavigation] = useState(false);

	const headerStyles = cx({
		[styles.headerFixed]: true,
		[styles.columnAligned]: true,
		[styles.redStyled]: true,
	});

	return (
		<header className={headerStyles} id="header">
			<Navigation isOpened={navOpen} closeNav={() => toggleNavigation(false)} />
			<Hamburger onClick={() => toggleNavigation(true)} />
			<p className={styles.logo}>TSO MINI</p>
			<p className={styles.divider} />
		</header>
	);
}
