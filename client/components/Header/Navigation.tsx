import React from 'react';
import Link from 'next/link';
import cx from 'classnames';
import { MdClose } from 'react-icons/md';
import styles from './navigation.module.scss';

interface NavigationProps {
	isOpened: boolean;
	closeNav: () => void;
}

export default function Navigation(props: NavigationProps): React.ReactElement {
	const { isOpened, closeNav } = props;
	const navigationStyles = cx({
		[styles.navigationContainer]: true,
		[styles.isOpened]: isOpened,
	});

	return (
		<div className={navigationStyles}>
			<header className={styles.navHeader}>
				<div />
				<MdClose onClick={closeNav} />
			</header>
			<nav className={styles.navigationWrapper}>
				<ul className={styles.linkList}>
					<Link href={'/'}>
						<a className={styles.listItem}>Menu</a>
					</Link>
					<Link href={'/order'}>
						<a className={styles.listItem}>Current Order</a>
					</Link>
					<Link href={'/profile'}>
						<a className={styles.listItem}>Profile</a>
					</Link>
					<Link href={'/profile/add-card'}>
						<a className={styles.listItem}>Add Card</a>
					</Link>
					<Link href={'/register'}>
						<a className={styles.listItem}>Register</a>
					</Link>
					<Link href={'/login'}>
						<a className={styles.listItem}>Login</a>
					</Link>
				</ul>
			</nav>
		</div>
	);
}
