import React, { useMemo } from 'react';
import { useStripe, useElements, CardElement } from '@stripe/react-stripe-js';
import styles from './styles.module.scss';
import useResponsiveFontSize from '../../utils/hooks/useResponsiveFontSize';

const useOptions = () => {
	const fontSize = useResponsiveFontSize();
	const options = useMemo(
		() => ({
			style: {
				base: {
					fontSize,
					color: '#424770',
					letterSpacing: '0.025em',
					fontFamily: 'Source Code Pro, monospace',
					'::placeholder': {
						color: '#aab7c4',
					},
				},
				invalid: {
					color: '#9e2146',
				},
			},
		}),
		[fontSize]
	);

	return options;
};

interface IProps {
	onSave: (token: string) => void;
}

const CardForm = (props: IProps): JSX.Element => {
	const stripe = useStripe();
	const elements = useElements();
	const options = useOptions();

	const handleSubmit = async (event): Promise<void> => {
		event.preventDefault();

		if (!stripe || !elements) {
			// Stripe.js has not loaded yet. Make sure to disable
			// form submission until Stripe.js has loaded.
			return;
		}

		const payload = await stripe.createToken(elements.getElement(CardElement));
		props.onSave(payload.token.id);
	};

	return (
		<form className={styles.addCardForm} onSubmit={handleSubmit}>
			{/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
			<label>
				Card details
				<CardElement
					options={options}
					onReady={() => {
						console.log('CardElement [ready]');
					}}
					onChange={(event) => {
						console.log('CardElement [change]', event);
					}}
					onBlur={() => {
						console.log('CardElement [blur]');
					}}
					onFocus={() => {
						console.log('CardElement [focus]');
					}}
				/>
			</label>
			<button type="submit" disabled={!stripe}>
				Add Card
			</button>
		</form>
	);
};

export default CardForm;
