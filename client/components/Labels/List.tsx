import React from 'react';
import styles from './styles.module.scss';

interface LabelListProps {
	labels: string[];
}

export default function LabelList(props: LabelListProps): React.ReactElement {
	console.log("LabelList ===>>> ", props.labels)
	return (
		<div className={styles.productLabelsInfoContainer}>
			{props.labels?.map((it) => (
				<p key={it} className={styles.productLabelInfo}>
					<span className={styles.productLabelInfoName}>{it}</span>
				</p>
			))}
		</div>
	);
}
