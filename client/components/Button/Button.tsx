import React from 'react';
import Isvg from 'react-inlinesvg';
import cx from 'classnames';
import styles from './styles.module.scss';

interface ButtonProps {
	onClick?: (ev: React.MouseEvent<HTMLButtonElement>) => any;
	label: string;
	iconLeft?: string;
	id?: string;
	style?: 'redBtn' | 'backBtn' | 'underlineStyle';
	googlePay?: boolean;
	disabled?: boolean;
}

export default function Button(props: ButtonProps): React.ReactElement {
	const classBtn = cx({
		['btn']: true,
		[styles.detailsButton]: true,
		[styles[props.style]]: true,
		[styles.googlePay]: props.googlePay,
		[styles.disabled]: props.disabled,
	});

	const iSvg = cx({
		[styles.iconLocateSvg]: !props.googlePay,
	});

	return (
		<button id={props.id} onClick={props.onClick} className={classBtn}>
			{props.iconLeft && (
				<span className={iSvg}>
					<Isvg src={props.iconLeft} />
				</span>
			)}
			<span>{props.label}</span>
		</button>
	);
}
