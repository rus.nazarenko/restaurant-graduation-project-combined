import axios from 'axios';
import config from '../config'
export const port = config.port

const getCookie = (variable) => {
  const allcookies = document.cookie
  const pos = allcookies.indexOf(`${variable}=`)
  if (pos !== -1) {
    var start = pos + variable.length + 1
    var end = allcookies.indexOf(';', start)

    if (end === -1) {
      end = allcookies.length
    }

    const value = allcookies.substring(start, end)
    return value
  }
}

export async function saveCard(token: string, user) {	
	return await axios.post(`http://localhost:${port}/api/payment-card`, { token }, {
			headers: { Cookie: getCookie("tso_token") },
			withCredentials: true
		});
}
