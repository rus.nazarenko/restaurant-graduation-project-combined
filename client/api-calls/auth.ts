import axios from 'axios';
import Cookie from 'js-cookie'
import config from '../config'
export const port = config.port



interface RegisterUser {
	email: string;
	password: string;
	firstName: string;
	lastName: string;
	phone_number: string;
}

export interface Card {
	id: number;
	external_id: string;
	user_id: number;
	created_at: Date;
	updated_at: Date;
	default: boolean;
}

export interface User extends RegisterUser {
	id: number;
	stripe_id?: string;
	card?: Card;
	created_at: Date;
	updated_at: Date;
}

const getCookie = (variable) => {
  const allcookies = document.cookie
  const pos = allcookies.indexOf(`${variable}=`)
  if (pos !== -1) {
    var start = pos + variable.length + 1
    var end = allcookies.indexOf(';', start)

    if (end === -1) {
      end = allcookies.length
    }

    const value = allcookies.substring(start, end)
    return value
  }
}

export async function authorize(user: RegisterUser): Promise<User> {
	const response = await axios.post(`http://localhost:${port}/api/register`, {
		...user,
	});
	Cookie.set('tso_token', response.data.access_token);
	// console.log("authorize response ===>>> ", response)
	return response.data;
}

export async function checkAuth(cookie?: string) {
	try {
		console.log("checkAuth cookie ===>>> ", cookie)
		const response = await axios.get(`http://localhost:${port}/api/auth`, {
			headers: { Cookie: cookie },
			withCredentials: true

		});
		console.log("checkAuth response ===>>> ", response)
		return response.data.user;
	} catch (e) {
		console.log(e.message);
		return {};
	}
}

export async function loginUser(body: { email: string; password: string }) {
	try {
		const response = await axios.post(`http://localhost:${port}/api/login`, body);
		Cookie.set('tso_token', response.data.user.access_token);
		// console.log('loginUser response ===>>> ', response.data);
		return response.data.user;
	} catch (e) {
		console.log(e.message);
		return {};
	}
}

export async function updateUser(user: User) {
	try {
		// console.log('updateing user ===>>> ', user);
		const response = await axios.patch(`http://localhost:${port}/api/user/${user.id}`, user,
	{headers: { Cookie: getCookie("tso_token") }, withCredentials: true	});
		// console.log('updateing user response ===>>> ', response.data);
		return response.data.user;
	} catch (e) {
		console.log(e);
		return {};
	}
}
