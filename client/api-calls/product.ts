import { fetcher } from './index';

export interface Label {
	name: string;
	url: string;
}

export interface Product {
	id: number;
	name: string;
	price: number;
	image: string;
	label_objects: Label[];
	description: string;
	created_at: Date;
	updated_at: Date;
}

const getCookie = (variable) => {
  const allcookies = document.cookie
  const pos = allcookies.indexOf(`${variable}=`)
  if (pos !== -1) {
    var start = pos + variable.length + 1
    var end = allcookies.indexOf(';', start)

    if (end === -1) {
      end = allcookies.length
    }

    const value = allcookies.substring(start, end)
    return value
  }
}

export function getProductList(url: string): Promise<Product[]> {
	
	return fetcher.get(url,
	{headers: { Cookie: getCookie("tso_token") }, withCredentials: true	})
	.then((res) => {
		return res.data
	});
}


export function getProductById(id: number): Promise<Product> {
	return fetcher.get(`/api/product/view/${id}`)
	.then((res) => {
		return res.data
	});
}
