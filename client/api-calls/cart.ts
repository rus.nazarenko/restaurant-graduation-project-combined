import axios from 'axios';
import { Product } from './product';
import config from '../config'
export const port = config.port


const getCookie = (variable) => {
  const allcookies = document.cookie
  const pos = allcookies.indexOf(`${variable}=`)
  if (pos !== -1) {
    var start = pos + variable.length + 1
    var end = allcookies.indexOf(';', start)

    if (end === -1) {
      end = allcookies.length
    }

    const value = allcookies.substring(start, end)
    return value
  }
}
	// const cookie = getCookie("tso_token")



export async function addProductToCart(cart_id: number, product: Product): Promise<unknown> {
	// console.log("addProductToCart ===>>> ", cart_id, product)
	const response = await axios.post(`http://localhost:${port}/api/cart/product`, {
		cart_id,
		product,
	},
	{headers: { Cookie: getCookie("tso_token") }, withCredentials: true	}
	);

	return response.data;
}

export async function getUserOrder(cookies): Promise<any> {
	// console.log("getUserOrder cookie ===>>> ", cookies)
	const response = await axios.get(`http://localhost:${port}/api/cart`, {
		headers: { Cookie: cookies },
	});
	// console.log("getUserOrder response ===>>> ", response.data.order)
	return response.data.order;
}

export async function payForOrder(order_id: number, card_id: number): Promise<any> {
	await axios.post(`http://localhost:${port}/api/cart/pay`, { card_id, order_id },
	{headers: { Cookie: getCookie("tso_token") }, withCredentials: true	});
}
