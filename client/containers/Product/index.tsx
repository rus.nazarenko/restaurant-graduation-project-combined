import { Product } from '../../api-calls/product';
import styles from './styles.module.scss';
import LabelList from '../../components/Labels/List';
import Button from '../../components/Button/Button';
import { formButtonText } from '../../utils/functions';
// import { addToCart } from '../../api-calls/cart';
import { addProductToCart as addToCart } from '../../api-calls/cart';

interface ProductContentProps {
	product: Product;
}

export function ProductContent(props: ProductContentProps): React.ReactElement {
	// console.log("ProductContent ===>>>", props.product);
	const { product } = props;
	return (
		<div>
			<div className={styles.containerModify}>
				<div className={styles.productModify}>
					<div className={styles.productImage}>
						<img src={product.image} alt={product.name} />
					</div>
					<div className={styles.fixedOrder}>
						<div className={styles.productInfo}>
							<div className={styles.productInfoWrap}>
								<p className={styles.productName}>{product.name}</p>
								<p className={styles.productSubName}>{product.description}</p>
							</div>
							<LabelList labels={product.label_objects.map((it) => it.name)} />
						</div>
					</div>
				</div>
				<div className={styles.addButton}>
					<Button
						style={'redBtn'}
						label={formButtonText(product.price)}
						onClick={() => addToCart(1, product.id)}
					/>
				</div>
			</div>
		</div>
	);
}
