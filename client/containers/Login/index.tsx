import Header from '../../components/Header';
import styles from './styles.module.scss';
import Input from '../../components/Input';
import { useState } from 'react';
import Button from '../../components/Button/Button';
import { loginUser } from '../../api-calls/auth';
import { useRouter } from 'next/router';

export default function LoginContainer(): JSX.Element {
	const [email, changeEmail] = useState('');
	const [password, changePassword] = useState('');
	const router = useRouter();

	function handleSubmit(ev) {
		ev.preventDefault();

		loginUser({ email, password }).then(() => router.push('/'));
	}
	return (
		<div className={styles.loginContainerWrapper}>
			<Header />
			<form onSubmit={handleSubmit} className={styles.loginContainer}>
				<div>
					<Input
						value={email}
						onChange={(e) => changeEmail(e.target.value)}
						label={'Enter your email'}
						placeholder={'Email'}
						name={'email'}
					/>
					<Input
						value={password}
						onChange={(e) => changePassword(e.target.value)}
						label={'Enter your password'}
						type={'password'}
						placeholder={'Password'}
						name={'password'}
					/>
				</div>
				<div className={styles.buttonWrapper}>
					<Button style={'redBtn'} label={'Login'} />
				</div>
			</form>
		</div>
	);
}
