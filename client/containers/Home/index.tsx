import React, { useEffect } from 'react';
import useSWR from 'swr';

import styles from './styles.module.scss';
import { getProductList, Product } from '../../api-calls/product';
import { MainProduct } from '../../components/MainProduct';
import { log } from '../../utils/logger';
import { checkAuth } from '../../api-calls/auth';

interface HomeComponentProps {
	order: unknown;
	addToCart: (product: Product) => void;
}

export function HomeComponent(props: HomeComponentProps): React.ReactElement {
	useEffect(() => {
		checkAuth().then(console.log);
	});
	const { data, error } = useSWR('/api/product/list?limit=5', getProductList);

	if (error) {
		log(error);
		return null;
	}

	return (
		<div className={styles.productContainerWrapper}>
			{data?.map((it) => (
				<MainProduct key={it.id} product={it} onAdd={props.addToCart} />
			))}
		</div>
	);
}
