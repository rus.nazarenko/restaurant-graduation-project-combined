import React, { useState } from 'react';
import styles from './styles.module.scss';
import Header from '../../components/Header';
import Input from '../../components/Input';
import Button from '../../components/Button/Button';
import { authorize } from '../../api-calls/auth';
import { useRouter } from 'next/router';

export default function LoginContainer(): JSX.Element {
	const [email, changeEmail] = useState('');
	const [password, changePassword] = useState('');
	const [repass, changeRePassword] = useState('');
	const router = useRouter();

	function handleSubmit(ev): void {
		ev.preventDefault();
		// console.log('submit');
		authorize({ email, password } as any).then(() => router.push('/'));
	}
	return (
		<div className={styles.registerContainerWrapper}>
			<Header />
			<form onSubmit={handleSubmit} className={styles.registerContainer}>
				<div>
					<Input
						value={email}
						onChange={(e) => changeEmail(e.target.value)}
						label={'Enter your email'}
						placeholder={'Email'}
						name={'email'}
					/>
					<Input
						value={password}
						onChange={(e) => changePassword(e.target.value)}
						label={'Enter your password'}
						type={'password'}
						placeholder={'Password'}
						name={'password'}
					/>
					<Input
						value={repass}
						onChange={(e) => changeRePassword(e.target.value)}
						label={'Repeat your password'}
						type={'password'}
						placeholder={'Repeated Password'}
						name={'repeat-password'}
					/>
				</div>
				<div className={styles.buttonWrapper}>
					<Button style={'redBtn'} label={'Register'} />
				</div>
			</form>
		</div>
	);
}
