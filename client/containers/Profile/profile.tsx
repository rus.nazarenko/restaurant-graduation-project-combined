import { updateUser, User } from '../../api-calls/auth';
import Header from '../../components/Header';
import styles from './profile.module.scss';
import Button from '../../components/Button/Button';
import Input from '../../components/Input';
import { useState } from 'react';
import Router, { useRouter } from 'next/router';

interface ProfileContainerProps {
	user: User;
}

export function ProfileContainer(props: ProfileContainerProps): JSX.Element {
	const user = props.user;

	const [email, changeEmail] = useState(user.email);
	const [phone_number, changePhoneNumber] = useState(user.phone_number);
	const [firstName, changeFirstName] = useState(user.firstName);
	const [lastName, changeLastName] = useState(user.lastName);
	const router = useRouter();

	async function handleUserUpdate(ev) {
		ev.preventDefault();
		await updateUser({
			...user,
			email,
			phone_number,
			firstName,
			lastName			
		});
		await router.push('/');
	}

	return (
		<>
			<Header />
			<div className={styles.userEditProfileContainer}>
				<div className={styles.profileContainer}>
					<form onSubmit={handleUserUpdate} className={styles.formWrapper}>
						<Input
							placeholder={'Email'}
							name={'email'}
							onChange={(e) => changeEmail(e.target.value)}
							label={'Email'}
							value={email}
						/>
						<Input
							onChange={(e) => changePhoneNumber(e.target.value)}
							label={'Phone number'}
							placeholder={'Phone number'}
							name={'phone_number'}
							value={phone_number}
						/>
						<Input
							onChange={(e) => changeFirstName(e.target.value)}
							value={firstName}
							label={'First Name'}
							name={'firstName'}
							placeholder={'First Name'}
						/>
						<Input
							onChange={(e) => changeLastName(e.target.value)}
							value={lastName}
							label={'Last Name'}
							name={'lastName'}
							placeholder={'Last Name'}
						/>
						<div className={styles.buttonsWrapper}>
							<Button label={'Save'} style={'redBtn'} />
						</div>
					</form>
				</div>
			</div>
		</>
	);
}
