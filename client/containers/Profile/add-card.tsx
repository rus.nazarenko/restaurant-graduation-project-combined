import React from 'react';
import cx from 'classnames';
import styles from './styles.module.scss';
import Header from '../../components/Header';
import CardForm from '../../components/Card/Input';
import {checkAuth} from '../../api-calls/auth'
import { saveCard } from '../../api-calls/card';
import { useRouter } from 'next/router';

export default function AddCard(): JSX.Element {
	const formClassName = cx(styles.insetButtons);
	const router = useRouter();
	const cardStyles = cx({
		[styles.card]: true,
		card: true,
	});

	function handleSaveCard(token): void {		
			saveCard(token).then(() => router.push('/'));		
	}

	return (
		<div className={cardStyles}>
			<Header />
			<div className={styles.container}>
				<div className={styles.creditCardInputs}>
					<div className={formClassName}>
						<CardForm onSave={handleSaveCard} />
					</div>
				</div>
			</div>
		</div>
	);
}
