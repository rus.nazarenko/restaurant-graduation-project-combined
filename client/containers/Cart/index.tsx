import { User } from '../../api-calls/auth';
import Header from '../../components/Header';
import Link from 'next/link';
import styles from './styles.module.scss';
import DatePicker from '../../components/DatePicker';
import ProductItem from '../../components/Cart/Product';
import Button from '../../components/Button/Button';
import { useRouter } from 'next/router';
import { payForOrder } from '../../api-calls/cart';

interface CartProps {
	user?: User;
	order: any;
}

export default function CartContainer(props: CartProps): JSX.Element {
	const router = useRouter();

	function backMenu() {
		router.push('/');
	}

	function handlePayment() {
		console.log("CartContainer props ===>>>", props)
		payForOrder(props.order.id, props.user.card.id).then(() => router.push('/'));
	}

	return (
		<div>
			<Header />
			<div className={styles.cartWrapperContainer}>
				<div className={styles.formWrapper}>
					<div className={styles.dateSelectionWrapper}>
						<p>Order Details</p>
						<div>
							<p>Order Time</p>
							<DatePicker />
						</div>
					</div>
					<div className={styles.productListWrapper}>
						{props.order.products
							.reduce((acc, product) => {
								let found = false;
								for (const it of acc) {
									if (product.id === it.product.id) {
										it.count++;
										found = true;
									}
								}
								if (!found) {
									acc.push({ product, count: 1 });
								}
								return acc;
							}, [])
							.map((it) => (
								<ProductItem key={it.id} product={it.product} count={it.count} />
							))}
					</div>

					<div className={styles.paymentOption}>
						{props.user.card ? (
							<p>You can pay for order - your card is added</p>
						) : (
							<Link href={'profile/add-card'}>
								<a>Add Card</a>
							</Link>
						)}
					</div>
					<div className={styles.buttonWrapper}>
						<Button onClick={backMenu} style={'underlineStyle'} label={'To menu'} />

						<Button onClick={handlePayment} style={'redBtn'} label={'Pay for order'} />
					</div>
				</div>
			</div>
		</div>
	);
}
