'use strict';
const { Model } = require('sequelize')

module.exports = (sequelize, DataTypes) => {
  class product_modifier extends Model {
    static associate(models) {
      this.belongsToMany(models.product_order, {
        through: 'product_modifier_to_product_order',
        foreignKey: 'product_modifier_id'
      })
    }
  };
  product_modifier.init({
    product_id: {
      type: DataTypes.INTEGER
    },
    modifier_id: {
      type: DataTypes.INTEGER
    }
  }, {
    sequelize,
    modelName: 'product_modifier',
    underscored: true
  });
  return product_modifier;
};