'use strict';
const {
  Model
} = require('sequelize')

module.exports = (sequelize, DataTypes) => {
  class product_modifier_to_product_order extends Model {
    static associate(models) {
    }
  };
  product_modifier_to_product_order.init({
    product_modifier_id: DataTypes.INTEGER,
    product_order_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'product_modifier_to_product_order',
    underscored: true
  });
  return product_modifier_to_product_order;
};