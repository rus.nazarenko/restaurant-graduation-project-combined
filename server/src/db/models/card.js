'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class card extends Model {
    static associate(models) {
      this.belongsTo(models.user)
    }
  };
  card.init({
    external_id: DataTypes.STRING,
    fingerprint: DataTypes.STRING,
    user_id: DataTypes.INTEGER,
    default_card: DataTypes.BOOLEAN,
  }, {
    sequelize,
    modelName: 'card',
    underscored: true
  });
  return card;
};