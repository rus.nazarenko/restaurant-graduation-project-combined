'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class product extends Model {
    static associate(models) {
      this.belongsTo(models.label)
      this.belongsTo(models.category)
      this.belongsToMany(models.modifier, { through: 'product_modifier' })
      this.belongsToMany(models.order, { through: 'product_order' })
    }
  };
  product.init({
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    price: {
      type: DataTypes.DECIMAL(10,2),
      allowNull: true
    },    
    description: {
      type: DataTypes.STRING,
      allowNull: true
    },
    category_id: {
      type: DataTypes.INTEGER
    },
    label_id: {
      type: DataTypes.INTEGER
    },
  }, {
    sequelize,
    modelName: 'product',
    underscored: true
  });
  return product;
};