'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class transaction extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  transaction.init({
    order_id: DataTypes.INTEGER,
    type: DataTypes.STRING,
    status: DataTypes.STRING,
    amount: DataTypes.DECIMAL,
    stripe_transaction_id: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'transaction',
    underscored: true
  });
  return transaction;
};