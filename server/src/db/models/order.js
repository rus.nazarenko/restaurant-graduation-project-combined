'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class order extends Model {
    static associate(models) {
      this.belongsTo(models.user)
      this.hasMany(models.transaction)
      this.hasMany(models.product_order)
      this.belongsToMany(models.product, { through: 'product_order' })
    }
  };
  order.init({
    status: {
      type: DataTypes.ENUM,
      values: ['', 'received', 'cooking', 'done', 'canceled'],
      defaultValue: ''
    },
    user_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'order',
    underscored: true
  });
  return order;
};