'use strict';
const { Model } = require('sequelize')

module.exports = (sequelize, DataTypes) => {
  class product_order extends Model {
    static associate(models) {
      this.belongsToMany(models.product_modifier, {
        through: 'product_modifier_to_product_order',
        foreignKey: 'product_order_id'
      })
    }
  };
  product_order.init({
    // id: {
    //   type: DataTypes.INTEGER,
    //   primaryKey: true,
    //   autoIncrement: true,
    //   allowNull: false
    // },
    order_id: DataTypes.INTEGER,
    product_id: DataTypes.INTEGER,
    product_quantity: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'product_order',
    underscored: true
  });
  return product_order;
};