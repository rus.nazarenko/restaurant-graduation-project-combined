'use strict';
const { Model } = require('sequelize')

module.exports = (sequelize, DataTypes) => {
  class modifier extends Model {
    static associate(models) {
      this.belongsToMany(models.product, { through: 'product_modifier' })
    }
  };
  modifier.init({
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    price: {
      type: DataTypes.DECIMAL(10,2),
      allowNull: true
    },
    allergen: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
  }, {
    sequelize,
    modelName: 'modifier',
    underscored: true
  });
  return modifier;
};