'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class user extends Model {
    static associate(models) {
      this.hasMany(models.card)
      this.hasMany(models.order)
    }
  };

  user.init(
    {
      // id: {
      //   type: DataTypes.UUID,
      //   // defaultValue: Sequelize.UUIDV4,
      //   // defaultValue: Sequelize.literal('uuid_generate_v4()'),
      //   allowNull: false,
      //   primaryKey: true
      // },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false
      },
      role: {
        type: DataTypes.ENUM,
        values: ['customer', 'admin'],
        defaultValue: 'customer'
      },
      phone_number: {
        type: DataTypes.STRING,
        allowNull: true
      },
      first_name: {
        type: DataTypes.STRING,
        allowNull: true
      },
      last_name: {
        type: DataTypes.STRING,
        allowNull: true
      },
      stripe_id: {
        type: DataTypes.STRING,
        allowNull: true
      },
      // created_at: {
      //   type: DataTypes.DATE
      // },
      // updated_at: {
      //   type: DataTypes.DATE
      // }
    },
    {
      sequelize,
      modelName: 'user',
      underscored: true,
      // freezeTableName: true // название таблицы не будет в множественном числе
    });
  return user;
};