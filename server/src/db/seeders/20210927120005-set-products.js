'use strict';
const { Op } = require('sequelize');
const { Sequelize } = require('../models');

const products = [
  {
    name: "Bourbon bbq chicken",
    price: 3.5,
    description: "House-made Bourbon BBQ sauce, bacon, BBQ chicken, balsamic-roasted red onions, pizza mozzarella and cheddar cheese. Finished with a buttermilk ranch drizzle.",
    label_id: 3,
    category_id: 1,
    created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
    updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
  },
  {
    name: "Royal-hawaiian",
    price: 4,
    description: "Sweet Thai honey garlic, Gouda, provolone, Parmesan, pizza mozzarella, red onions, smoked prosciutto, bacon, pineapple, and toasted sesame seeds.",
    label_id: 4,
    category_id: 1,
    created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
    updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
  },
  {
    name: "Tropical chicken",
    price: 4,
    description: "Alfredo sauce, pizza mozzarella, cheddar, bacon, spicy chicken breast and pineapple.",
    label_id: 5,
    category_id: 1,
    created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
    updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
  },
  {
    name: "Tacos",
    price: 3.2,
    description: "Three grilled soft tacos filled with lettuce, red onions, cilantro, cheddar and tomatoes with a hint of lime, topped with crunchy tortilla strips, our creamy sweet chili sauce and your choice of breaded jalapeño shrimp, breaded or grilled chicken breast.",
    label_id: 7,
    category_id: 3,
    created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
    updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
  },
  {
    name: "Boston brute",
    price: 3.4,
    description: "Genoa salami, pepperoni, smoked ham, pizza mozzarella, onions and our signature pizza sauce.",
    label_id: 6,
    category_id: 3,
    created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
    updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
  },
  {
    name: "The big dipper",
    price: 4.4,
    description: "Sliced steak, roasted red peppers, pizza mozzarella, and panko crusted onion rings. Served with horseradish mayo and au jus.",
    label_id: 8,
    category_id: 3,
    created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
    updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
  },
  {
    name: "Aquafina",
    price: 1,
    description: "Aquafina® Bottled Water (591ml)",
    label_id: 1,
    category_id: 2,
    created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
    updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
  },
  {
    name: "Brisk",
    price: 1.2,
    description: "Brisk® Iced Tea 591 mL Bottle.",
    label_id: 2,
    category_id: 2,
    created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
    updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
  },
]



module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert("products", products)
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("products", {
      email: {
        [Op.in]: products.map((it) => it.name),
      },
    });
  }
}