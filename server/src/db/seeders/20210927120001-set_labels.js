'use strict';
const { Op } = require('sequelize');
const { Sequelize } = require('../models');

const labels = [
  {
    // id: 1,
    name: "aquafina",
    image: "products/beverages/aquafina.jpg",
    created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
    updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
  },
  {
    name: "brisk",
    image: "products/beverages/brisk.jpg",
    created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
    updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
  },

  {
    name: "bourbon bbq chicken",
    image: "products/pizza/bourbon_bbq_chicken.jpg",
    created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
    updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
  },
  {
    name: "royal hawaiian",
    image: "products/pizza/royal_hawaiian.jpg",
    created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
    updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
  },
  {
    name: "tropical chicken",
    image: "products/pizza/tropical_chicken.jpg",
    created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
    updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
  },

  {
    name: "boston brute",
    image: "products/sandwiches/boston_brute.jpg",
    created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
    updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
  },
  {
    name: "tacos",
    image: "products/sandwiches/tacos.jpg",
    created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
    updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
  },
  {
    name: "the big dipper",
    image: "products/sandwiches/the_big_dipper.jpg",
    created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
    updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
  }
]

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert("labels", labels)
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("labels", {
      email: {
        [Op.in]: labels.map((it) => it.name),
      },
    });
  }
}