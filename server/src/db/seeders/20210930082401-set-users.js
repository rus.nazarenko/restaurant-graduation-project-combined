'use strict';
const {Op} = require('sequelize');
const { Sequelize } = require('../models');

const users = [
  {
    first_name: "admin",
    last_name: "admin",
    email: "admin@mail.com",
    password: "123456",
    role: "admin",
    phone_number: "+380681234567",
    stripe_id: "",
    created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
    updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
  },
  {
    first_name: "Ruslan",
    last_name: "Nazarenko",
    email: "ruslan@mail.com",
    password: "123456",
    role: "customer",
    phone_number: "+380689004099",
    stripe_id: "",
    created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
    updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
  },
  {
    first_name: "Kirill",
    last_name: "Ivanov",
    email: "Kirill@mail.com",
    password: "123456",
    role: "customer",
    phone_number: "+380686543211",
    stripe_id: "",
    created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
    updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
  },
  {
    first_name: "Maria",
    last_name: "Petrova",
    email: "Maria@mail.com",
    password: "123123",
    role: "customer",
    phone_number: "+380633216548",
    stripe_id: "",
    created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
    updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
  }
]

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert("users", users)
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("users", {
      email: {
        [Op.in]: users.map((it) => it.email),
      },
    });
  }
}