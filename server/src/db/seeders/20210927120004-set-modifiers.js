'use strict';
const { Op } = require('sequelize');
const { Sequelize } = require('../models');

const modifiers = [
  {
    name: "nuts",
    price: 3.541,
    allergen: true,
    created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
    updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
  },
  {
    name: "sauce",
    price: 1,
    allergen: true,
    created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
    updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
  },
  {
    name: "mayonnaise",
    price: 1,
    allergen: true,
    created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
    updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
  },
  {
    name: "mushrooms",
    price: 2,
    allergen: true,
    created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
    updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
  },
  {
    name: "pepper",
    price: 0.5,
    allergen: true,
    created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
    updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
  },
  {
    name: "tomatoes",
    price: 1.5,
    allergen: false,
    created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
    updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
  },
]


module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert("modifiers", modifiers)
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("modifiers", {
      email: {
        [Op.in]: modifiers.map((it) => it.name),
      },
    });
  }
}
