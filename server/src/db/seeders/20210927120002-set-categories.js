'use strict';
const { Op } = require('sequelize');
const { Sequelize } = require('../models');

const categories = [
  {
    name: "Pizza",
    created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
    updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
  },
  {
    name: "Beverages",
    created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
    updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
  },
  {
    name: "Sandwiches",
    created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
    updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
  },
]

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert("categories", categories)
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("categories", {
      email: {
        [Op.in]: categories.map((it) => it.name),
      },
    });
  }
}
