// import { User } from './authTypes'
import { Request, Response, NextFunction } from 'express'
import jwt from 'jsonwebtoken'
import { secret } from '../config/server'
import { getCookie } from '../utils/getCookie'
import db from '../db/models/index'

interface DecodedToken {
  id: number,
  email: string
}

export const adminVerificationMW = async (req: Request, res: Response, next: NextFunction) => {
  try {
    if (!req.headers.cookie) throw new Error("No token")
    let decodedToken = jwt.verify(getCookie("tso_token", req.headers.cookie), secret)
    const decodedToken2: DecodedToken = JSON.parse(JSON.stringify(decodedToken))
    const user = await db.user.findOne({
      where: { id: decodedToken2.id, email: decodedToken2.email, role: 'admin' }
    })
    if (user) next()
    else throw new Error("Not a valid token ")
  } catch (err) {
    res.status(401).json({ success: false, error: err.message })
  }
}