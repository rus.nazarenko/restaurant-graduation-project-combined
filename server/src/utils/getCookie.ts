export const getCookie = (variable, allcookies) => {
  const pos = allcookies.indexOf(`${variable}=`)

  if (pos !== -1) {
    const start = pos + variable.length + 1
    let end = allcookies.indexOf(';', start)

    if (end === -1) end = allcookies.length

    const value = allcookies.substring(start, end)
    return value
  }
}