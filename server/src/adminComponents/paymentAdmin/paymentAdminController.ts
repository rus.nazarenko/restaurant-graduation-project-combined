import { refundPaymentAdminService } from './paymentAdminService'
import { RefundDB, RefundStripe } from './paymentAdminTypes'
import { Request, Response } from 'express'
import { secretStripe } from '../../config/stripe'
import Stripe from 'stripe'
const stripe = new Stripe(secretStripe, { apiVersion: '2020-08-27' })


export const refundPaymentAdminController = async (req: Request, res: Response) => {
  try {
    if (!Object.keys(req.body).length) { throw new Error("No data") }

    const refundStripe: RefundStripe = {
      amount: req.body.amount,
      charge: req.body.charge
    }

    const refund = await stripe.refunds.create(refundStripe)

    const payload: RefundDB = {
      charge_id: req.body.charge,
      status: refund.status,
      amount: refund.amount,
      stripe_transaction_id: refund.id,
      type: refund.object
    }

    const transaction = await refundPaymentAdminService(payload)

    res.status(200).json(transaction)
  } catch (err) {
    res.status(401).json({ success: false, error: err.message })
  }
}