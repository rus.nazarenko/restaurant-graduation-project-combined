export interface RefundDB {
  charge_id?: string,
  status: string,
  amount: number,
  stripe_transaction_id: string,
  type: string,
  order_id?: number
}

export interface RefundStripe {
  amount: number,
  charge: string
}