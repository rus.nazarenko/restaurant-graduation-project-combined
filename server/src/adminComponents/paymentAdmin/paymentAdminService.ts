import db from '../../db/models'
import { RefundDB } from './paymentAdminTypes'


export const refundPaymentAdminService = async (payload: RefundDB) => {
  try {
    const oldTransaction = await db.transaction.findOne({
      where: { stripe_transaction_id: payload.charge_id }
    })

    const order_id: number = oldTransaction.toJSON().order_id

    const newTransactionPayload: RefundDB = {
      status: payload.status,
      amount: payload.amount,
      stripe_transaction_id: payload.stripe_transaction_id,
      order_id,
      type: payload.type
    }

    const newTransaction = await db.transaction.create(newTransactionPayload)

    return newTransaction.toJSON()
  } catch (err) {
    throw new Error(err)
  }
}