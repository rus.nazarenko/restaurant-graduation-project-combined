import express from 'express'
const paymentAdminRouter = express.Router()
import { refundPaymentAdminController } from './paymentAdminController'


paymentAdminRouter.post('/refund', refundPaymentAdminController)


export default paymentAdminRouter