export interface Product {
  name: string,
  price: number,
  description: string,
  label_id: number,
  category_id: number
}