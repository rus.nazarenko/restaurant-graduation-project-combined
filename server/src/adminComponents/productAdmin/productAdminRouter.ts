import express from 'express'
const productAdminRouter = express.Router()
import { addProductAdminController } from './productAdminController'


productAdminRouter.post('/', addProductAdminController)


export default productAdminRouter