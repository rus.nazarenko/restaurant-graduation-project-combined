import db from '../../db/models'
import { Product } from './productAdminTypes'


export const addProductAdminService = async (newProduct: Product) => {
  try {
    const product = await db.product.create(newProduct)

    return product
  } catch (err) {
    throw new Error(err)
  }
}