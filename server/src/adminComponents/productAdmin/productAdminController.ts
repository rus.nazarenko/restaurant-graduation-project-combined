import { addProductAdminService } from './productAdminService'
import { Product } from './productAdminTypes'
import { Request, Response } from 'express'


export const addProductAdminController = async (req: Request, res: Response) => {
  try {
    if (!req.body) { throw new Error("No data ") }

    const newProduct: Product = {
      name: req.body.name,
      price: req.body.price,
      description: req.body.description,
      label_id: req.body.label_id || null,
      category_id: req.body.category_id || null
    }

    const product = await addProductAdminService(newProduct)

    res.status(200).json(product)
  } catch (err) {
    res.status(401).json({ success: false, error: err.message })
  }
}