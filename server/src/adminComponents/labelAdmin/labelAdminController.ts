import { addLabelAdminService } from './labelAdminService'
import { Label } from './labelAdminTypes'
import { Request, Response } from 'express'



export const addLabelAdminController = async (req: Request, res: Response) => {
  try {
    if (!req.body.name) { throw new Error("No data") }
    if (Object.entries(req.file).length === 0) { throw new Error("No file") }

    const newlabel: Label = {
      name: req.body.name,
      image: `products/${req.body.category}/${req.file.originalname}`
    }

    const label = await addLabelAdminService(newlabel)
    res.status(201).json(label)
  } catch (err) {
    res.status(401).json({ success: false, error: err.message })
  }
}
