import express from 'express'
const labelAdminRouter = express.Router()
import { addLabelAdminMW } from '../../middleware/addLabelAdminMW'
import { addLabelAdminController } from './labelAdminController'


labelAdminRouter.post('/', addLabelAdminMW, addLabelAdminController)



export default labelAdminRouter