import db from '../../db/models'
import { Label } from './labelAdminTypes'


export const addLabelAdminService = async (newProduct: Label) => {
  try {
    const label = await db.label.create(newProduct)

    return label
  } catch (err) {
    throw new Error(err)
  }
}