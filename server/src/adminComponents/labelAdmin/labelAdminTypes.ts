export interface Label {
  name: string,
  image: string
}