import db from '../../db/models'


export const getUserListAdminService = async () => {
  try {
    let userList = await db.user.findAll()

    return userList
  } catch (err) {
    throw new Error(err)
  }
}