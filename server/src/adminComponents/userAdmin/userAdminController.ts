import { getUserListAdminService } from './userAdminService'
// import { User } from './userTypes'
import { Request, Response } from 'express'



export const getUserListAdminController = async (req: Request, res: Response) => {
  try {
    const userList = await getUserListAdminService()

    res.status(200).json(userList)
  } catch (err) {
    res.status(401).json({ success: false, error: err.message })
  }
}