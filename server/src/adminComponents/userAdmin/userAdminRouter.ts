import express from 'express'
const userAdminRouter = express.Router()
import { getUserListAdminController } from './userAdminController'


userAdminRouter.get('/', getUserListAdminController)



export default userAdminRouter