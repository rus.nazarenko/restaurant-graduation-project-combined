export interface User {
  email: string;
  password: string;
}

export interface Token {
  id: number;
  email: string
}