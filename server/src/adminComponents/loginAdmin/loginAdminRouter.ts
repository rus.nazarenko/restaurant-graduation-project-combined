import express from 'express'
const loginAdminRouter = express.Router()
import { loginAdminController } from './loginAdminController'


loginAdminRouter.post('/login', loginAdminController)



export default loginAdminRouter