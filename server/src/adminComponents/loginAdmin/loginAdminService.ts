import { User, Token } from './loginAdminTypes'
import db from '../../db/models'
import jwt from 'jsonwebtoken'
import { secret } from '../../config/server'




export const loginAdminService = async (data: User) => {
  try {
    let currenUser = await db.user.findOne({ where: { email: data.email } })

    if (!currenUser) throw new Error("Wrong email")
    currenUser = currenUser.toJSON()

    if (data.password !== currenUser.password) throw new Error("Wrong password")
    if (currenUser.role !== 'admin') throw new Error("No access")

    const dataToken: Token = {
      id: currenUser.id,
      email: currenUser.email
    }
    const token = jwt.sign(dataToken, secret)

    currenUser = {
      access_token: token,
      ...currenUser.dataValues,
      firstName: currenUser.first_name,
      lastName: currenUser.last_name,
    }

    return currenUser
  } catch (err) {
    throw new Error(err)
  }
}