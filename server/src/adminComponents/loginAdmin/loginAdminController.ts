import { loginAdminService } from './loginAdminService'
import { User } from './loginAdminTypes'
import { Request, Response } from 'express'



export const loginAdminController = async (req: Request, res: Response) => {
  try {
    if (Object.entries(req.body).length === 0) { throw new Error("No data") }

    const data: User = {
      email: req.body.email,
      password: req.body.password
    }
    const user = await loginAdminService(data)
    res.status(200).json({ user })

  } catch (err) {
    res.status(401).json({ success: false, error: err.message })
  }
}