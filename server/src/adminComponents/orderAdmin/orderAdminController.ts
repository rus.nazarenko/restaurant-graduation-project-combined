import { getOrderListAdminService, setOrderAdminService } from './orderAdminService'
import { Status } from './orderAdminTypes'
import { Request, Response } from 'express'



export const getOrderListAdminController = async (req: Request, res: Response) => {
  try {
    if (!req.params.userId) { throw new Error("No params") }

    const orderList = await getOrderListAdminService(Number(req.params.userId))

    res.status(200).json(orderList)
  } catch (err) {
    res.status(401).json({ success: false, error: err.message })
  }
}


export const setOrderAdminController = async (req: Request, res: Response) => {
  try {
    if (!req.body) { throw new Error("No data") }

    const newStatus: Status = {
      id: req.body.orderId,
      status: req.body.status
    }

    const order = await setOrderAdminService(newStatus)

    res.status(200).json(order)
  } catch (err) {
    res.status(401).json({ success: false, error: err.message })
  }
}