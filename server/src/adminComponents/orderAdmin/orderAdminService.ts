import db from '../../db/models'
import { Status } from './orderAdminTypes'

export const getOrderListAdminService = async (userId: number) => {
  try {
    let orderList = await db.order.findAll({ where: { user_id: userId } })
    const orderListCombined = []

    for (let item of orderList) {
      const products = await item.getProducts()
      item = { ...item.toJSON(), products: [...products] }
      orderListCombined.push(item)
    }

    return orderListCombined
  } catch (err) {
    throw new Error(err)
  }
}


export const setOrderAdminService = async (newStatus: Status) => {
  try {
    const order = await db.order.findByPk(newStatus.id)

    // if (order.toJSON().status !== 'received') {
    //   throw new Error("The order has not been paid!")
    // }

    order.status = newStatus.status
    const newOrder = await order.save({ fields: ['status'] })

    return newOrder
  } catch (err) {
    throw new Error(err)
  }
}