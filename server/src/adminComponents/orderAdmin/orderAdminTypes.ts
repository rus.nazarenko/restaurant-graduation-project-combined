export enum StrStatus {
  '',
  'received',
  'cooking',
  'done',
  'canceled'
}


export interface Status {
  id: number,
  status: string
}