import express from 'express'
const orderAdminRouter = express.Router()
import { getOrderListAdminController, setOrderAdminController } from './orderAdminController'


orderAdminRouter.get('/:userId', getOrderListAdminController)
orderAdminRouter.post('/', setOrderAdminController)



export default orderAdminRouter