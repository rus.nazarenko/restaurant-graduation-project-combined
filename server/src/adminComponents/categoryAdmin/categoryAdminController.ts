import { addCategoryAdminService } from './categoryAdminService'
import { Request, Response } from 'express'


export const addCategoryAdminController = async (req: Request, res: Response) => {
  try {
    if (!req.body) { throw new Error("No data ") }

    const category = await addCategoryAdminService(req.body.name)

    res.status(200).json(category)
  } catch (err) {
    res.status(401).json({ success: false, error: err.message })
  }
}