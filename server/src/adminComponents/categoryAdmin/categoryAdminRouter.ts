import express from 'express'
const addCategoryAdminRouter = express.Router()
import { addCategoryAdminController } from './categoryAdminController'


addCategoryAdminRouter.post('/', addCategoryAdminController)



export default addCategoryAdminRouter