import db from '../../db/models'


export const addCategoryAdminService = async (name: string) => {
  try {
    const category = await db.category.create({ name })

    return category
  } catch (err) {
    throw new Error(err)
  }
}