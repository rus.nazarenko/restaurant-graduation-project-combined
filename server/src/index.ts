import express, { json, Application } from 'express'
import cookieParser from 'cookie-parser'
import cors from 'cors'
import { port } from './config/server'
import authRouter from "./customerComponents/auth/authRouter"
import userRouter from "./customerComponents/user/userRouter"
import productRouter from "./customerComponents/product/productRouter"
import orderRouter from "./customerComponents/order/orderRouter"
import cardRouter from "./customerComponents/card/cardRouter"
import loginAdminRouter from "./adminComponents/loginAdmin/loginAdminRouter"
import productAdminRouter from "./adminComponents/productAdmin/productAdminRouter"
import categoryAdminRouter from "./adminComponents/categoryAdmin/categoryAdminRouter"
import userAdminRouter from "./adminComponents/userAdmin/userAdminRouter"
import orderAdminRouter from "./adminComponents/orderAdmin/orderAdminRouter"
import labelAdminRouter from "./adminComponents/labelAdmin/labelAdminRouter"
import paymentAdminRouter from "./adminComponents/paymentAdmin/paymentAdminRouter"
import { customerVerificationMW } from "./middleware/customerVerificationMW"
import { adminVerificationMW } from './middleware/adminVerificationMW'

import { logging } from './middleware/logging'

const app: Application = express()


app.use(cors({ credentials: true, origin: true }))
app.use(logging())
app.use(json())
app.use(cookieParser())
app.use(express.static('static'))


app.use('/api', authRouter)
app.use('/api/cart', customerVerificationMW, orderRouter)
app.use('/api/product', productRouter)
app.use('/api/user', customerVerificationMW, userRouter)
app.use('/api/payment-card', customerVerificationMW, cardRouter)


app.use('/api/adminpanel', loginAdminRouter)
app.use('/api/adminpanel/product', adminVerificationMW, productAdminRouter)
app.use('/api/adminpanel/category', adminVerificationMW, categoryAdminRouter)
app.use('/api/adminpanel/user', adminVerificationMW, userAdminRouter)
app.use('/api/adminpanel/order', adminVerificationMW, orderAdminRouter)
app.use('/api/adminpanel/label', adminVerificationMW, labelAdminRouter)
app.use('/api/adminpanel/payment', adminVerificationMW, paymentAdminRouter)


app.listen(port, () => {
  console.log(`The server is running on http://localhost:${port}`)
})