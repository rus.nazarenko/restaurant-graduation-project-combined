import express from 'express'
const userRouter = express.Router()
import { updateUserController } from './userController'


userRouter.patch('/:id', updateUserController)



export default userRouter