import { User } from "./userTypes"
import db from '../../db/models'


export const updateUserService = async (data: User) => {
  try {

    await db.user.update(data, {
      where: { id: data.id }
    })

    let user = await db.user.findByPk(data.id)
    user = {
      ...user.toJSON(),
      firstName: user.first_name,
      lastName: user.last_name,
    }

    return user
  } catch (err) {
    throw new Error(err)
  }
}


export const getUserService = async (data: number) => {
  try {
    const user = await db.user.findByPk(data)
    return user.toJSON()
  } catch (err) {
    throw new Error(err)
  }
}