import { updateUserService } from './userService'
import { User } from './userTypes'
import { Request, Response } from 'express'



export const updateUserController = async (req: Request, res: Response) => {
  try {
    if (Object.entries(req.body).length === 0) { throw new Error("No data") }

    const data: User = {
      id: Number(req.params.id),
      email: req.body.email,
      phone_number: req.body.phone_number,
      first_name: req.body.firstName,
      last_name: req.body.lastName,
    }

    const user = await updateUserService(data)

    res.status(201).json({ user })
  } catch (err) {
    res.status(401).json({ success: false, error: err.message })
  }
}