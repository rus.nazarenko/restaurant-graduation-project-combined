export interface NewProduct {
  cart_id: number,
  product_id: number,
  modifiers?: [] | null
}

export interface DelProduct {
  order_id: number,
  product_id: number
}

export interface PayloadCharge {
  source: string,
  amount: number,
  currency: string,
  customer: any,
  description: string
}


export interface SetOrder {
  order_id: number,
  status: string,
  amount: number,
  stripe_transaction_id: string,
  type: string
}