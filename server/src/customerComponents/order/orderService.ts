import { Op } from 'sequelize'
import { port } from '../../config/server'
const imageURL = `http://localhost:${port}`
import db from '../../db/models'
import { NewProduct, DelProduct, SetOrder } from './orderTypes'


export const getContentsCartService = async (user_id: number) => {
  try {
    let currentOrderSqlz = await db.order.findOne(
      { where: { [Op.and]: [{ user_id, status: '' }] } })

    let order = {}

    if (!currentOrderSqlz) {
      // =================== Create a new empty cart ===================
      currentOrderSqlz = await db.order.create({ user_id })
      order = { ...currentOrderSqlz.toJSON(), products: [] }
    } else {
      //  ================ Get old products from the cart ================
      // get all products from the cart
      let products = []
      const productOrderSqlzArr = await currentOrderSqlz.getProducts()
      // add all images for these products 
      for (const item of productOrderSqlzArr) {
        const currentItem = item.toJSON()

        const labelSqlz = await db.label.findByPk(currentItem.label_id)
        currentItem.image = `${imageURL}/${labelSqlz.toJSON().image}`

        for (let i = 0; i < currentItem.product_order.product_quantity; i++) {
          products.push(currentItem)
        }
      }
      order = { ...currentOrderSqlz.toJSON(), products }
    }
    return order
  } catch (err) {
    throw new Error(err)
  }
}


export const addProductToCartService = async (newProduct: NewProduct) => {
  try {
    // 1. ============== Add a new product to the cart or increment the quantity  ==============
    const currentOrderSqlz = await db.order.findByPk(newProduct.cart_id)
    const currentProductSqlz = await db.product.findByPk(newProduct.product_id)
    // check if this product is in cart 
    const productInCartSqlzArr = await currentOrderSqlz.getProducts({ joinTableAttributes: ['product_quantity'] })
    // if the product exists in the cart, then increment
    let indicator = false

    for (const item of productInCartSqlzArr) {
      if (item.toJSON().id === newProduct.product_id && !newProduct.modifiers) {
        indicator = true
        const currentProductOrderSqlz = await db.product_order.findOne({
          where: { order_id: newProduct.cart_id, product_id: item.toJSON().id }
        })
        await currentProductOrderSqlz.increment('product_quantity')
      }
    }
    // if the product does not exist in the cart, then add it 
    let productOrderSqlz
    if (!indicator) {
      productOrderSqlz = await currentOrderSqlz.addProduct(currentProductSqlz, { through: { product_quantity: 1 } })
      productOrderSqlz = productOrderSqlz[0].toJSON()

      // Set modifiers to product to order
      if (newProduct.modifiers) {
        const product_orderSqlz = await db.product_order.findOne({ where: productOrderSqlz })

        for (const item of newProduct.modifiers) {
          const product_modifierSqlz = await db.product_modifier.findByPk(item)
          const modifierToProductToOrderSqlz = await product_orderSqlz.addProduct_modifier(product_modifierSqlz)
        }
      }
    }
    return true

  } catch (err) {
    throw new Error(err)
  }
}


export const getSumTotal_getStripeUserService = async (id) => {
  try {
    const order = await db.order.findByPk(id)
    const productTotal = await order.getProducts()

    let sumTotal = 0
    productTotal.forEach((item) => {
      item = item.toJSON()
      sumTotal += item.price * item.product_order.product_quantity
    })

    const user = await db.user.findByPk(order.user_id)

    return { sumTotal, stripeUser: user.toJSON().stripe_id }
  } catch (err) {
    throw new Error(err)
  }
}


export const setOrderService = async ({ order_id, status, amount, stripe_transaction_id, type }: SetOrder) => {
  try {
    const currentOrder = await db.order.findByPk(order_id)
    await currentOrder.createTransaction({ status, amount, stripe_transaction_id, type }) //status: succeeded | pending | failed

    if (status === "succeeded") {
      currentOrder.status = 'received'
      await currentOrder.save({ fields: ['status'] })
    }

    return true
  } catch (err) {
    throw new Error(err)
  }
}


export const delProductFromCartService = async (payload: DelProduct) => {
  try {
    const product_order = await db.product_order.findOne({
      where: { product_id: payload.product_id, order_id: payload.order_id }
    })

    if (product_order.product_quantity > 1) {
      await product_order.decrement('product_quantity')
    }
    else {
      await db.product_order.destroy({
        where: {
          product_id: payload.product_id,
          order_id: payload.order_id
        }
      })
    }
    return true
  } catch (err) {
    throw new Error(err)
  }
}


export const getStatusCartService = async (payload: number) => {
  try {
    const order = await db.order.findByPk(payload)
    return order
  } catch (err) {
    throw new Error(err)
  }
}


export const getOrderListService = async (payload: number) => {
  try {
    const orders = await db.order.findAll({ where: { user_id: payload } })
    const result = []

    for (let item of orders) {
      const productList = await item.getProducts()

      const order = {
        ...item.toJSON(),
        products: [...productList]
      }

      result.push(order)
    }
    return result
  } catch (err) {
    throw new Error(err)
  }
}