const payload = {
  name: "Name",
  email: "name@gmail.com",
  password: "24i5342nf4vv84vbo"
}

const responseBody = {
  id: '6086542009326603604006',
  name: "Name",
  email: "name@gmail.com",
  password: "24i5342nf4vv84vbo"
}

jest.mock('../../db/models', () => ({
  user: {
    findOne: jest.fn(),
    create: jest.fn()
    // findOne: jest.fn({ where: { email: payload.email } })
  }
}))
const db = require('../../db/models')
const { registerUserService } = require('./userService')

describe('User service', () => {
  describe('registerUserService', () => {
    it('should call findOne method', async () => {
      await registerUserService(payload)
      expect(db.user.findOne).toBeCalled()
      expect(db.user.findOne).toBeCalledWith({ where: { email: payload.email } })

      // expect(db.user.findOne).toBeCalled()
      // expect(db.user.findOne).toBeCalledWith(url, keys, buildReq())
      // expect(db.user.findAll).toHaveBeenCalled()
      // expect(db.user.create).toHaveBeenCalledWith(user)
      // expect(db.user.findOne).toHaveBeenCalledWith({ where: { email } })
      // expect(typeof res === 'string').toBeTruthy();
      //     expect(res).toEqual(responseBody.id)
    })
    it('should call create method', async () => {
      await registerUserService(payload)
      expect(db.user.create).toBeCalled()
      expect(db.user.create).toBeCalledWith(payload)
    })

  })
})

// ===================================================================
// const responseBody = {
//   id: '6086542009326603604006',
//   status: 'PENDING'
// };

// jest.mock('../../CybersourceCore', () => ({
//   cybersource: {
//     post: jest.fn(() => responseBody),
//   },
//   DEFAULT_CURRENCY: 'USD'
// }));

// const Core = require('../../CybersourceCore');
// const { createRefund } = require('../../Payment');

// const reversalAmount = 10;
// const paymentID = '123asd';
// const url = `/pts/v2/payments/${paymentID}/refunds`;

// function buildReq() {
//   return {
//     clientReferenceInformation: {
//       code: `refund_${paymentID}`
//     },
//     orderInformation: {
//       amountDetails: {
//         totalAmount: reversalAmount,
//         currency: Core.DEFAULT_CURRENCY
//       }
//     }
//   }
// }

// describe('Payments: Create Refund function', () => {
//   it('create refund should call cybersource function with params', async function (done) {
//     const keys = {};
//     await createRefund(paymentID, reversalAmount, keys);
//     expect(Core.cybersource.post).toBeCalled();
//     expect(Core.cybersource.post).toBeCalledWith(url, keys, buildReq());
//     done();
//   });

//   it('create refund should return id', async function (done) {
//     const keys = {};
//     const res = await createRefund(paymentID, reversalAmount, keys);
//     expect(res).toBeTruthy();

//     expect(typeof res === 'string').toBeTruthy();
//     expect(res).toEqual(responseBody.id);
//     done();
//   });
// })