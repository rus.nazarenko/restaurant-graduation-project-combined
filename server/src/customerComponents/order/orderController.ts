import {
  getContentsCartService, addProductToCartService, getSumTotal_getStripeUserService,
  delProductFromCartService, setOrderService, getStatusCartService, getOrderListService
} from './orderService'
import { NewProduct, DelProduct, PayloadCharge, SetOrder } from './orderTypes'
import { Request, Response } from 'express'
import { secretStripe, currency } from '../../config/stripe'
import Stripe from 'stripe'
const stripe = new Stripe(secretStripe, { apiVersion: '2020-08-27' })



export const getContentsCartController = async (req: Request, res: Response) => {
  try {
    // if (!req.headers.cookie) throw new Error()
    // const decodedToken = jwt.verify(getCookie("tso_token", req.headers.cookie), secret)

    const order = await getContentsCartService(res.locals.user_id)

    res.status(201).json({ success: true, order })
  } catch (err) {
    res.status(401).json({ success: false, error: err.message })
  }
}


export const addProductToCartController = async (req: Request, res: Response) => {
  try {
    if (Object.entries(req.body).length === 0) { throw new Error("No data") }

    const newProduct: NewProduct = {
      cart_id: req.body.cart_id,
      product_id: req.body.product.id,
      modifiers: req.body.product.modifiers ? req.body.product.modifiers : null
    }

    const order = await addProductToCartService(newProduct)

    res.status(201).json({ order })
  } catch (err) {
    res.status(401).json({ success: false, error: err.message })
  }
}


export const delProductFromCartController = async (req: Request, res: Response) => {
  try {
    if (Object.entries(req.body).length === 0) { throw new Error("No data") }

    const payload: DelProduct = {
      order_id: req.body.cart_id,
      product_id: req.body.product_id
    }

    await delProductFromCartService(payload)

    res.status(200).json({ success: true })
  } catch (err) {
    res.status(401).json({ success: false, error: err.message })
  }
}


export const payController = async (req: Request, res: Response) => {
  try {
    if (!req.body) { throw new Error("No data ") }

    // 1. Get the total amount and id of the stypeUser
    const sumTotal_stripeUser = await getSumTotal_getStripeUserService(req.body.order_id)

    // 2. Make a payment to the stripe
    const payloadCharge: PayloadCharge = {
      source: req.body.card_id,
      amount: Math.round(sumTotal_stripeUser.sumTotal * 100),
      currency,
      customer: sumTotal_stripeUser.stripeUser,
      description: `User payment of order ${req.body.order_id}`,
    }

    const charge = await stripe.charges.create(payloadCharge)

    // 3. Write the payment result to tables: transaction and order
    const serOrder: SetOrder = {
      order_id: req.body.order_id,
      status: charge.status,
      amount: charge.amount,
      stripe_transaction_id: charge.id,
      type: charge.object
    }

    await setOrderService(serOrder)

    res.status(201).json({ paid: true })
  } catch (err) {
    res.status(401).json({ success: false, error: err.message })
  }
}


export const getStatusCartController = async (req: Request, res: Response) => {
  try {
    if (Object.entries(req.params).length === 0) { throw new Error("No data") }

    const order = await getStatusCartService(Number(req.params.id))

    res.status(201).json({ order })
  } catch (err) {
    res.status(401).json({ success: false, error: err.message })
  }
}


export const getOrderListController = async (req: Request, res: Response) => {
  try {
    const orderList = await getOrderListService(res.locals.user_id)

    res.status(201).json({ orderList })
  } catch (err) {
    res.status(401).json({ success: false, error: err.message })
  }
}