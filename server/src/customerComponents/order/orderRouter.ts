import express from 'express'
const productRouter = express.Router()
import {
  getContentsCartController, addProductToCartController, getOrderListController,
  payController, delProductFromCartController, getStatusCartController
} from './orderController'

productRouter.get('/', getContentsCartController)
productRouter.post('/product', addProductToCartController)
productRouter.delete('/product', delProductFromCartController)
productRouter.post('/pay', payController)
productRouter.get('/status/:id', getStatusCartController)
productRouter.get('/overview', getOrderListController)


export default productRouter