import { PayloadGetCard, DelCard, AddCard } from "./cardTypes"
import db from '../../db/models'


export const addCardService = async (payload: AddCard) => {
  try {
    const user = await db.user.findByPk(payload.user_id)
    const cards = await db.card.findAll({ where: { user_id: payload.user_id } })

    if (!cards.length) payload.newCard.default_card = true
    let card = await user.createCard(payload.newCard)
    card = card.toJSON()

    return card
  } catch (err) {
    throw new Error(err)
  }
}

export const getCardsService = async (payload: PayloadGetCard) => {
  try {

    let cards = await db.card.findAll({
      where: {
        user_id: payload.user_id
      }
    })

    return cards
  } catch (err) {
    throw new Error(err)
  }
}


export const delCardService = async (payload: DelCard) => {
  try {

    const resultCard = await db.card.destroy({ where: { id: payload.card_id } })

    const card = await db.card.findOne({ where: { user_id: payload.user_id } })
    if (card) {
      card.default_card = true
      await card.save({ fields: ['default_card'] })
    }

    if (!resultCard) throw new Error("There is no such credit card")

    return true
  } catch (err) {
    throw new Error(err)
  }
}