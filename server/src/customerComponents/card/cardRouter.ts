import express from 'express'
const cardRouter = express.Router()
import { addCardController, delCardController } from './cardController'


cardRouter.post('/', addCardController)
cardRouter.delete('/', delCardController)



export default cardRouter