export interface PayloadGetCard {
  fingerprint: string,
  user_id: number
}

export interface DelCard {
  user_id: number,
  card_id: number
}


export interface AddCard {
  newCard: {
    external_id: string,
    fingerprint: string,
    default_card?: boolean
  },
  user_id: number
}