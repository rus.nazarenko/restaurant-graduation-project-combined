import { addCardService, getCardsService, delCardService } from './cardService'
import { getUserService, updateUserService } from '../user/userService'
import { PayloadGetCard, DelCard, AddCard } from './cardTypes'
import { Request, Response } from 'express'
import jwt from 'jsonwebtoken'
import { secret } from '../../config/server'
import { getCookie } from '../../utils/getCookie'
import { secretStripe } from '../../config/stripe'
import Stripe from 'stripe'
const stripe = new Stripe(secretStripe, { apiVersion: '2020-08-27' })



export const addCardController = async (req: Request, res: Response) => {
  try {
    if (Object.entries(req.body).length === 0) { throw new Error("No data") }
    const decodedToken = jwt.verify(getCookie("tso_token", req.headers.cookie), secret)
    const token = JSON.parse(JSON.stringify(decodedToken))

    // 1. check if the stripe_id field exists
    let user = await getUserService(token.id)

    // 1.1 If the stripe_id field does not exist . ===>>> stripe.customers.create()    
    if (!user.stripe_id) {
      const params: Stripe.CustomerCreateParams = { email: user.email }
      const customerStripe: Stripe.Customer = await stripe.customers.create(params)
      user = await updateUserService({ ...user, stripe_id: customerStripe.id })
    }

    // 2 If stripe_id field exists  ===>>> create a credit card stripe.customers.createSource()
    const card = await stripe.customers.createSource(user.stripe_id, { source: req.body.token })

    let fingerprint: string = ''
    if (card.object === 'card') fingerprint = card.fingerprint
    else throw new Error("no fingerprint")

    // 3. Get all customer cards 
    let cards
    const payload: PayloadGetCard = {
      fingerprint,
      user_id: user.id
    }
    cards = await getCardsService(payload)

    // 4. If such a card already exists, we return it 
    let responseCard
    let indicator: boolean = false

    cards.forEach((item) => {
      if (item.fingerprint === fingerprint) {
        responseCard = item
        indicator = true
      }
    })

    // 5. If such a card does not exist, write it to the database and return it 
    if (!indicator) {
      const payloadAddCardService: AddCard = {
        newCard: {
          external_id: card.id,
          fingerprint
        },
        user_id: user.id
      }
      responseCard = await addCardService(payloadAddCardService)
    }

    res.status(201).json({ responseCard })
  } catch (err) {
    res.status(401).json({ success: false, error: err.message })
  }
}


export const delCardController = async (req: Request, res: Response) => {
  try {
    if (Object.entries(req.body).length === 0) { throw new Error("No data") }

    const payload: DelCard = {
      user_id: res.locals.user_id,
      card_id: req.body.card_id
    }

    await delCardService(payload)

    res.status(201).json({ success: true })
  } catch (err) {
    res.status(401).json({ success: false, error: err.message })
  }
}