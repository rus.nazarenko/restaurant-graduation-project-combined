import { User, RespUser, DataToken } from "./authTypes"
import db from '../../db/models'
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import { secret } from '../../config/server'


export const registerUserService = async (data: User) => {
  try {
    const currenUser = await db.user.findOne({ where: { email: data.email } })
    if (currenUser) throw new Error("This email is already registered")
    data.password = await bcrypt.hash(data.password, 10)
    const result = await db.user.create(data)

    const dataToken: DataToken = {
      id: result.toJSON().id,
      email: result.toJSON().email
    }

    const token: string = jwt.sign(dataToken, secret)

    const resp: RespUser = {
      email: result.dataValues.email,
      password: data.password,
      access_token: token
    }

    return resp
  } catch (err) {
    throw new Error(err)
  }
}


export const loginUserService = async (data: User) => {
  try {
    const currenUser = await db.user.findOne({ where: { email: data.email } })
    if (!currenUser) throw new Error("Wrong email")
    const passwordTrue = await bcrypt.compare(data.password, currenUser.password)
    if (!passwordTrue) throw new Error("Wrong password")

    const dataToken: DataToken = {
      id: currenUser.toJSON().id,
      email: currenUser.toJSON().email
    }
    const token: string = jwt.sign(dataToken, secret)

    const user = {
      access_token: token,
      ...currenUser.toJSON(),
      firstName: currenUser.toJSON().first_name,
      lastName: currenUser.toJSON().last_name,
    }

    return user
  } catch (err) {
    throw new Error(err)
  }
}


export const authUserService = async (decodedToken) => {
  try {
    let currenUser = await db.user.findByPk(decodedToken.id)
    currenUser = currenUser.toJSON()

    let defaultCard = await db.card.findOne({ where: { user_id: decodedToken.id, default_card: true } })

    currenUser = {
      ...currenUser,
      firstName: currenUser.first_name,
      lastName: currenUser.last_name,
      card: defaultCard ? { id: defaultCard.toJSON().external_id } : null
    }

    return currenUser
  } catch (err) {
    throw new Error(err)
  }
}