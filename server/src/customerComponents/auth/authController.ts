import { registerUserService, loginUserService, authUserService } from './authService'
import { User, RespUser } from './authTypes'
import { Request, Response } from 'express'
import jwt from 'jsonwebtoken'
import { secret } from '../../config/server'
import { getCookie } from '../../utils/getCookie'


export const registerUserController = async (req: Request, res: Response) => {
  try {
    if (Object.entries(req.body).length === 0) { throw new Error("No data") }

    const data: User = {
      email: req.body.email,
      password: req.body.password
    }

    const newUser: RespUser = await registerUserService(data)

    res.status(201).json(newUser)
  } catch (err) {
    res.status(401).json({ success: false, error: err.message })
  }
}


export const loginUserController = async (req: Request, res: Response) => {
  try {
    if (Object.entries(req.body).length === 0) { throw new Error("No data") }

    const data: User = {
      password: req.body.password,
      email: req.body.email
    }
    const user = await loginUserService(data)
    res.status(200).json({ user })

  } catch (err) {
    res.status(401).json({ success: false, error: err.message })
  }
}


export const authUserController = async (req: Request, res: Response) => {
  try {
    if (!req.headers.cookie) throw new Error("No token")
    const decodedToken = jwt.verify(getCookie("tso_token", req.headers.cookie), secret)
    let user = await authUserService(decodedToken)

    res.status(200).json({ user })
  } catch (err) {
    res.status(401).json({ success: false, error: err.message })
  }
}