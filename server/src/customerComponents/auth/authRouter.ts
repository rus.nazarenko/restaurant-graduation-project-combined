import express from 'express'
const userRouter = express.Router()
import { registerUserController, loginUserController, authUserController } from './authController'


userRouter.post('/register', registerUserController)
userRouter.post('/login', loginUserController)
userRouter.get('/auth', authUserController)



export default userRouter