export interface User {
  email: string;
  password: string
}

export interface RespUser {
  email: string;
  password: string;
  access_token: string
}

export interface DataToken {
  id: number;
  email: string
}