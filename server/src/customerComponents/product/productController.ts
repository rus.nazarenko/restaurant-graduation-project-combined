import { getProductListService, getProductService } from './productService'
import { Request, Response } from 'express'



export const getProductListController = async (req: Request, res: Response) => {
  try {
    const productList = await getProductListService(Number(req.query.limit))

    res.status(201).json(productList)
  } catch (err) {
    res.status(401).json({ success: false, error: err.message })
  }
}


export const getProductController = async (req: Request, res: Response) => {
  try {
    const product = await getProductService(Number(req.params.id))

    res.status(200).json(product)
  } catch (err) {
    res.status(401).json({ success: false, error: err.message })
  }
}