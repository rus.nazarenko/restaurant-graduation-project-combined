import db from '../../db/models'
const imageURL = 'http://localhost:3001'


export const getProductListService = async (limit: number) => {
  try {
    let productList = await db.product.findAll({ limit })
    const productListArr = []
    for (let item of productList) {
      //get modifiers      
      const currentProductModifier = await item.getModifiers()
      item = item.toJSON()
      item.labels = []
      currentProductModifier.forEach((elem) => {
        item.labels.push(elem.toJSON().name)
      })
      // get images
      const currentImage = await db.label.findByPk(item.label_id)
      item.image = `${imageURL}/${currentImage.image}`
      productListArr.push(item)
    }
    return productListArr
  } catch (err) {
    throw new Error(err)
  }
}


export const getProductService = async (id: number) => {
  try {
    //get product
    const currenProduct = await db.product.findByPk(id)
    const finalProduct = currenProduct.toJSON()

    // get images to product
    const currentImage = await db.label.findByPk(currenProduct.label_id)
    finalProduct.image = `${imageURL}/${currentImage.toJSON().image}`

    //get modifiers to product 
    const currenProductModifies = await currenProduct.getModifiers()

    finalProduct.label_objects = []
    currenProductModifies.forEach((item) => {
      finalProduct.label_objects.push(item.toJSON())
    })

    return finalProduct
  } catch (err) {
    throw new Error(err)
  }
}