import express from 'express'
const productRouter = express.Router()
import { getProductListController, getProductController } from './productController'


productRouter.get('/list/', getProductListController)
productRouter.get('/view/:id', getProductController)



export default productRouter