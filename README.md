### Application launch 
cd server
docker-compose up --build -d

cd ..
cd client
npm i
npm run dev

in the browser go to  http://localhost:3000


###### New admin routes: ######

## Login
POST http://localhost:3001/api/adminpanel/login
Request example:
{
  "email": "admin@mail.com",
  "password": "123456"
}


## Add product
POST http://localhost:3001/api/adminpanel/product
Request example:
{
  "name": "pie ",
  "price": "2",
  "description": "new pie",
  "category_id": 3
}
Headers: Cookie: tso_token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiZW1haWwiOiJhZG1pbkBtYWlsLmNvbSIsImlhdCI6MTYzNDU2MDMxMn0.Zl7i8cEzNUwLJnixM07I5iDRu48rwiZf3D1mhQ81t-M


## Add category
POST http://localhost:3001/api/adminpanel/category
Request example:
{
  "name": "salads"
}
Headers: Cookie: tso_token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiZW1haWwiOiJhZG1pbkBtYWlsLmNvbSIsImlhdCI6MTYzNDU2MDMxMn0.Zl7i8cEzNUwLJnixM07I5iDRu48rwiZf3D1mhQ81t-M


## Get user list
GET http://localhost:3001/api/adminpanel/user
Request example:
Headers: Cookie: tso_token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiZW1haWwiOiJhZG1pbkBtYWlsLmNvbSIsImlhdCI6MTYzNDU2MDMxMn0.Zl7i8cEzNUwLJnixM07I5iDRu48rwiZf3D1mhQ81t-M


## Get order list for user
GET http://localhost:3001/api/adminpanel/order/5  (userId)
Request example:
Headers: Cookie: tso_token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiZW1haWwiOiJhZG1pbkBtYWlsLmNvbSIsImlhdCI6MTYzNDU2MDMxMn0.Zl7i8cEzNUwLJnixM07I5iDRu48rwiZf3D1mhQ81t-M


## Set order status
POST http://localhost:3001/api/adminpanel/order
Request example:
{
  "orderId": "1",
  "status": "cooking"
}
Headers: Cookie: tso_token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiZW1haWwiOiJhZG1pbkBtYWlsLmNvbSIsImlhdCI6MTYzNDU2MDMxMn0.Zl7i8cEzNUwLJnixM07I5iDRu48rwiZf3D1mhQ81t-M


## Add label
POST http://localhost:3001/api/adminpanel/label
Request example:

form-data:
name  big-Mac
category  sandwiches
image <file>

Headers: Cookie: tso_token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiZW1haWwiOiJhZG1pbkBtYWlsLmNvbSIsImlhdCI6MTYzNDU2MDMxMn0.Zl7i8cEzNUwLJnixM07I5iDRu48rwiZf3D1mhQ81t-M


## Refund
POST http://localhost:3001/api/adminpanel/payment/refund
Request example:
{
  "amount": 150,
  "charge": "ch_3JmykcAlBluhlwQx0ByCkn3C"
}
Headers: Cookie: tso_token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiZW1haWwiOiJhZG1pbkBtYWlsLmNvbSIsImlhdCI6MTYzNDU2MDMxMn0.Zl7i8cEzNUwLJnixM07I5iDRu48rwiZf3D1mhQ81t-M



###### New customer routes: ######

## Adding a product with modifiers
POST http://localhost:3001/api/cart/product
Request example:
{
  "cart_id": 1,
  "product": {
    "id": 3,
    "name": "Bourbon bbq chicken",
    "price": "3.5",
    "description": "House-made Bourbon BBQ sauce, bacon, BBQ chicken, balsamic-roasted red onions, pizza mozzarella and cheddar cheese. Finished with a buttermilk ranch drizzle.",
    "category_id": 1,
    "label_id": 3,
    "createdAt": "2021-10-20T06:33:59.627Z",
    "updatedAt": "2021-10-20T06:33:59.627Z",
    "categoryId": 1,
    "labelId": 3,
    "image": "http://localhost:3001/products/pizza/bourbon_bbq_chicken.jpg",
    "labels": [
      "nuts",
      "sauce",
      "mayonnaise",
      "mushrooms",
      "pepper",
      "tomatoes"
    ],
    "modifiers": [3, 5]
  }
}
Headers: Cookie: tso_token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NSwiZW1haWwiOiJydXNAbWFpbC5jb20iLCJpYXQiOjE2MzQ3MjE2MDB9.-crf1y2tSq9CmHG5lorho1LCMGnl-sb7I4uMddEuH7M

## Remove a product from the cart
DELETE http://localhost:3001/api/cart/product
Request example:
{
  "cart_id": "1",
  "product_id": "3"
}
Headers: Cookie: tso_token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NSwiZW1haWwiOiJydXNAbWFpbC5jb20iLCJpYXQiOjE2MzQ3MjE2MDB9.-crf1y2tSq9CmHG5lorho1LCMGnl-sb7I4uMddEuH7M

## Remove credit card
DELETE http://localhost:3001/api/payment-card/
Request example:
{
  "card_id": "1"
}
Headers: Cookie: tso_token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NSwiZW1haWwiOiJydXNAbWFpbC5jb20iLCJpYXQiOjE2MzQ3MjE2MDB9.-crf1y2tSq9CmHG5lorho1LCMGnl-sb7I4uMddEuH7M

## Order status
GET http://localhost:3001/api/cart/status/1
Request example:
Headers: Cookie: tso_token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NSwiZW1haWwiOiJydXNAbWFpbC5jb20iLCJpYXQiOjE2MzQ3MjE2MDB9.-crf1y2tSq9CmHG5lorho1LCMGnl-sb7I4uMddEuH7M

## History of orders
GET http://localhost:3001/api/cart/overview
Request example:
Headers: Cookie: tso_token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NSwiZW1haWwiOiJydXNAbWFpbC5jb20iLCJpYXQiOjE2MzQ3MjE2MDB9.-crf1y2tSq9CmHG5lorho1LCMGnl-sb7I4uMddEuH7M